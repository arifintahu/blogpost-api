import { User, Profile, Post  } from "../orm";

interface userProfile {
    username : string;
    password : string;
    email : string;
}

export async function createUser(user : userProfile){
    try{
        let result = await User.create({
            username : user.username,
            password : user.password,
            status : 'pending'
        });
        let profileResult = await Profile.create({
            userid : result.get("userid"),
            email : user.email
        });
        console.log(result.get("userid"));
        return(profileResult);
    }catch (e){
        console.log(e);
        return(false);
    }
}

export async function readUserAll(){
    let result = await User.findAll({
        attributes : ['userid', 'username'],
        include : [{
            model : Profile,
            attributes : ['email']
        }]
    });
    if(result.length == 0){
        return(false);
    }else{
        return(result)
    }
}

export async function readUser(userid){
    let result = await User.findOne({
        where : {
            userid : userid
        },
        attributes : ['userid', 'username'],
        include : [{
            model : Profile,
            attributes : ['email']
        }]
    });
    if(!result){
        return(false);
    }else{
        return(result)
    }
}

export async function updateUser(userid : Int8Array){

}

export async function deleteUser(userid : Int8Array){

}