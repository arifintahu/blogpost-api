import * as express from "express";
import * as bodyParser from "body-parser";
import { createServer } from "http";
import { Server } from "net";
import { createUserAPI, readUserAllAPI,
        readUserAPI } from "./main.service";

const PORT = process.env.PORT || 3000;

const app = express();

app.use(bodyParser.urlencoded({ extended : true }));
app.use(bodyParser.json());
app.set("port", PORT);

app.get("/", (req, res) => {
    res.send("Test");
});

app.post("/profiles", createUserAPI);
app.get("/profiles", readUserAllAPI);
app.get("/profiles/:userid", readUserAPI);



const server = createServer(app);

export function startServer(): Server {
    return server.listen(PORT, () => {
        console.log("Server listens on port ", PORT);
    });
}