import { Sequelize, Model, DataTypes } from "sequelize";

const db = new Sequelize({
  database: process.env["POSTGRES_DATABASE"] || "blogpost",
  username: process.env["POSTGRES_USERNAME"] || "postgres",
  password: process.env["POSTGRES_PASSWORD"] || "postgres",
  host: process.env["POSTGRES_HOST"] || "localhost",
  port: parseInt(process.env["POSTGRES_PORT"], 10) || 5432,
  dialect: "postgres",
  logging: false,
  timezone: "Asia/Jakarta"
});

export class User extends Model {}
User.init(
  {
    userid : {
      type : DataTypes.INTEGER,
      autoIncrement : true,
      primaryKey : true
    },
    username : {
      type : DataTypes.STRING,
      allowNull : false
    },
    password : {
      type : DataTypes.STRING,
      allowNull : false
    },
    status : {
      type : DataTypes.ENUM,
      values : ['active', 'inactive', 'pending']
    }
  },
  { modelName: "user", sequelize: db }
);

export class Profile extends Model{}
Profile.init(
  {
    userid : {
      type : DataTypes.INTEGER,
      allowNull : false,
      primaryKey : true
    },
    email : {
      type : DataTypes.STRING,
      allowNull : false
    }
  },
  { modelName: "profile", sequelize : db}
)

export class Post extends Model{}
Post.init(
  {
    userid : {
      type : DataTypes.INTEGER,
      allowNull : false
    },
    postid : {
      type : DataTypes.INTEGER,
      autoIncrement : true,
      primaryKey : true
    },
    title : DataTypes.TEXT,
    body : DataTypes.TEXT,
    tag : {
      type : DataTypes.ENUM,
      values : ['economy', 'technology', 'social', 'politics']
    }
  },
  { modelName: "post", sequelize : db}
)

User.belongsTo(Profile, { targetKey : 'userid', foreignKey : 'userid'})

export function syncDB(): Promise<Sequelize> {
    return db.sync();
  }
  