import { createUser, readUserAll, readUser,
        updateUser, deleteUser } from "./mainfunc/user";

import { Request, Response } from "express";

export async function createUserAPI(req : Request, res : Response){
 let user = req.body;
 let result = await createUser(user);
 if(!result){
     res.status(404).send({ ok : false });
 }{
     res.send(result);
 }
}

export async function readUserAllAPI(req : Request, res : Response){
    let result =  await readUserAll();
    if(!result){
        res.status(404).send({ ok : false });
    }{
        res.send(result);
    }
}

export async function readUserAPI(req : Request, res : Response){
    let userid = req.params.userid;
    let result =  await readUser(userid);
    if(!result){
        res.status(404).send({ ok : false });
    }{
        res.send(result);
    }
}

export async function updateUserAPI(req : Request, res : Response){
    
}

export async function deleteUserAPI(req : Request, res : Response){
    
}