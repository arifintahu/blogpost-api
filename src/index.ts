import { startServer } from "./server";
import { syncDB } from "./orm";

async function startApp(){
    await syncDB();
    startServer();
}

startApp();